import numpy as np
from scipy.stats import binned_statistic


def bin_edge2_bin_center(bin_edges):
    center = np.zeros(len(bin_edges)-1)
    for i in range(len(bin_edges)-1):
        center[i] = bin_edges[i] + (bin_edges[i+1]-bin_edges[i])/2
    return center


def bin_center2_bin_edge(bin_center):
    edges = np.zeros(len(bin_center)+1)
    bin_sep = (bin_center[1]-bin_center[0])/2  # assume fixed bin width
    for i in range(len(bin_center)):
        #         print (i,bin_center[i])
        edges[i] = bin_center[i] - bin_sep
    edges[-1] = bin_center[-1] + (bin_center[-1]-bin_center[-2])/2
    return edges


def bin_data_given_bin_center(x, y, bin_center=[], nbin=10, Range=None, opt='mean'):
    '''
    Bin data, specifying the bin center
    arg :
    1. x : xdata
    2. y : ydata
    3. bin_center = an array of bin_center (default is None)
    4. nbin = 10 if the bin_center is None
    5. Range = None , given in the form (min(x),max(x))
    6. opt = sum (for statistics)
    count : compute the count of points within each bin. This is identical to an unweighted histogram. values array is not referenced.
    sum : compute the sum of values for points within each bin. This is identical to a weighted histogram.
    mean: compute the mean of the y-values in the same bin: useful for weighted data.
    std: compute the error bar
    Return:
    1. bin_center
    2. bin_counter
    '''

    if len(bin_center) != 0:
        bin_edges = bin_center2_bin_edge(bin_center)
    else:
        bin_edges = np.linspace(min(x), max(x), nbin)

    ret = binned_statistic(x, y, statistic=opt, bins=bin_edges, range=Range)
    return bin_edge2_bin_center(ret.bin_edges), ret.statistic
